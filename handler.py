import datetime as dt
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
import logging
import os
from smtplib import SMTP_SSL, SMTPAuthenticationError
from ssl import create_default_context
import sys

import boto3
from packt.api import PacktAPIClient
from packt.claimer import claim_product
from packt.constants import (
    DATE_FORMAT,
    PACKT_URL,
    PACKT_RECAPTCHA_SITE_KEY,
    SUCCESS_EMAIL_SUBJECT,
    SUCCESS_EMAIL_BODY
)
from packt.utils.anticaptcha import solve_recaptcha

	
client = boto3.client('ssm')


def get_secret(key):
	resp = client.get_parameter(
		Name='/Packt/{}'.format(key),
		WithDecryption=True
	)

	return resp['Parameter']['Value']


ANTICAPTCHA_API_KEY = get_secret('anticaptcha-api-key')
PACKT_USERNAME = get_secret('packt-username')
PACKT_PASSWORD = get_secret('packt-password')
SMTP_LOGIN = get_secret('smtp-login')
SMTP_PASSWORD = get_secret('smtp-password')
SMTP_HOST = get_secret('smtp-host')
SMTP_PORT = get_secret('smtp-port')
SENDER = get_secret('sender')
RECIPIENT = get_secret('recipient')

# Handle logging globally.
logger = logging.getLogger("packt")
logger.propagate = False
log_format = logging.Formatter('{levelname:^8s} | {message}', style='{')

std_handler = logging.StreamHandler(sys.stdout)
std_handler.setLevel(logging.INFO)
std_handler.setFormatter(log_format)

logger.addHandler(std_handler)
logger.setLevel(logging.DEBUG)


def create_email_message(sender, recipient, subject, body):
    message = MIMEMultipart()
    message['From'] = sender
    message['To'] = recipient
    message['Date'] = formatdate(localtime=True)
    message['Subject'] = subject
    message.attach(MIMEText(body))
    return message


def send_email(user, password, host, port, message):
    try:
        server = SMTP_SSL(host, port, context=create_default_context())
        server.login(user, password)
        logger.info('Sending email from {} to {} ...'.format(message['From'], message['To']))
        server.send_message(message)
        logger.info('Email to {} has been succesfully sent'.format(message['To']))
        server.quit()
        server.close()
    except SMTPAuthenticationError as e:
        print(e)


def lambda_handler(event, ctx):
    recaptcha_solution = solve_recaptcha(ANTICAPTCHA_API_KEY, PACKT_URL, PACKT_RECAPTCHA_SITE_KEY)
    api_client = PacktAPIClient({
        'username': PACKT_USERNAME,
        'password': PACKT_PASSWORD,
        'recaptcha': recaptcha_solution
    })

    product_data = claim_product(api_client, recaptcha_solution)

    send_email(
        SMTP_LOGIN,
        SMTP_PASSWORD,
        SMTP_HOST,
        SMTP_PORT,
        create_email_message(
            SENDER,
            RECIPIENT,
            SUCCESS_EMAIL_SUBJECT.format(
                dt.datetime.now().strftime(DATE_FORMAT),
                product_data['title']
            ),
            SUCCESS_EMAIL_BODY.format(product_data['title'])
        )
    )
